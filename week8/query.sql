select * from Shippers;

select ShipperName, count(OrderID)
from Orders join Shippers where Orders.ShipperID = Shippers.ShipperID
group by Shippers.ShipperID;

select CustomerName, OrderID from Customers left join Orders on Customers.CustomerID=Orders.CustomerID order by OrderID;

select FirstName, LastName, OrderID from Orders join Employees on Orders.EmployeeID=Employees.EmployeeID order by OrderID;